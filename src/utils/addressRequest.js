const addressRequest = (cep) => {
  return fetch(`https://viacep.com.br/ws/${cep}/json/`, {
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  })
    .then((data) => data.json())
    .catch((err) => console.log(err))
}

export default addressRequest
