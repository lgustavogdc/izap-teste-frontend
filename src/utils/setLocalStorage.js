const setLocalStorage = (address) => {
  localStorage.setItem('cep', address.cep)
  localStorage.setItem('logradouro', address.logradouro)
  address.complemento !== ''
    ? localStorage.setItem('complemento', address.complemento)
    : localStorage.removeItem('complemento')
  localStorage.setItem('bairro', address.bairro)
  localStorage.setItem('localidade', address.localidade)
  localStorage.setItem('uf', address.uf)
}

export default setLocalStorage
