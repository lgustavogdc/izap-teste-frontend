import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import AddressForm from './pages/AddressForm/AddressForm'
import Registered from './pages/Registered/Registered'

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact={true} component={AddressForm} />
          <Route path="/registered" component={Registered} />
        </Switch>
      </Router>
    </div>
  )
}

export default App
