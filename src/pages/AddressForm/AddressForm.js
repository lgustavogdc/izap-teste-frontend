import React, { useState } from 'react'
import addressRequest from '../../utils/addressRequest'
import setLocalStorage from '../../utils/setLocalStorage'
import { withRouter } from 'react-router-dom'
import './AddressForm.css'

function AddressForm(props) {
  const [data, setData] = useState({
    cep: localStorage.getItem('cep') ? localStorage.getItem('cep') : '',
    logradouro: localStorage.getItem('logradouro')
      ? localStorage.getItem('logradouro')
      : '',
    complemento: localStorage.getItem('complemento')
      ? localStorage.getItem('complemento')
      : '',
    bairro: localStorage.getItem('bairro')
      ? localStorage.getItem('bairro')
      : '',
    localidade: localStorage.getItem('localidade')
      ? localStorage.getItem('localidade')
      : '',
    uf: localStorage.getItem('uf') ? localStorage.getItem('uf') : '',
  })

  const handleCEPCompletion = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
    if (e.target.value.length > 8) {
      e.target.value = e.target.value.slice(0, -1)
    }
    if (e.target.value.length === 8) {
      setTimeout(() => {
        addressRequest(e.target.value).then((items) => {
          setData(items)
        })
      }, 200)
    }
  }

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    setLocalStorage(data)
    return props.history.push('/registered')
  }

  return (
    <div>
      <h1 className="address-form-title">Cadastre o seu endereço: </h1>
      <form onSubmit={handleSubmit} className="address-form">
        <div className="input">
          <label htmlFor="CEP">CEP: </label>
          <input
            type="number"
            placeholder="CEP"
            name="CEP"
            onChange={handleCEPCompletion}
            defaultValue={
              data.cep !== undefined ? data.cep.replace(/[^0-9]/, '') : ''
            }
          />
        </div>
        <div className="input">
          <label htmlFor="logradouro">Rua/Avenida: </label>
          <input
            type="text"
            name="logradouro"
            placeholder="Rua/Avenida"
            value={data.logradouro}
            onChange={handleChange}
          />
        </div>
        <div className="input">
          <label htmlFor="complemento">Complemento: </label>
          <input
            type="text"
            name="complemento"
            placeholder="Complemento"
            value={data.complemento}
            onChange={handleChange}
          />
        </div>
        <div className="input">
          <label htmlFor="bairro">Bairro: </label>
          <input
            type="text"
            name="bairro"
            placeholder="Bairro"
            value={data.bairro}
            onChange={handleChange}
          />
        </div>
        <div className="input">
          <label htmlFor="localidade">Cidade: </label>
          <input
            type="text"
            name="localidade"
            placeholder="Cidade"
            value={data.localidade}
            onChange={handleChange}
          />
        </div>
        <div className="input">
          <label htmlFor="uf">Estado: </label>
          <input
            type="text"
            name="uf"
            maxLength="2"
            placeholder="Estado"
            value={data.uf}
            onChange={handleChange}
          />
        </div>
        <input type="submit" value="Cadastrar" className="form-button" />
      </form>
    </div>
  )
}

export default withRouter(AddressForm)
