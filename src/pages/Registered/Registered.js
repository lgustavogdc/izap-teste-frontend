import React from 'react'
import { withRouter } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import './Registered.css'

const Registered = (props) => {
  const handleGoBack = () => {
    return props.history.push('/')
  }
  return (
    <div>
      <FontAwesomeIcon
        icon={faArrowLeft}
        onClick={handleGoBack}
        className="register-go-back"
      />
      <h1 className="register-success">Você foi registrado meu caro!</h1>
      <div className="data">
        <h3>Seus dados são: </h3>
        <ul className="register-list">
          <li>CEP: {localStorage.getItem('cep')}</li>
          <li>Rua/Avenida: {localStorage.getItem('logradouro')}</li>
          <li>Complemento: {localStorage.getItem('complemento')}</li>
          <li>Bairro: {localStorage.getItem('bairro')}</li>
          <li>Cidade: {localStorage.getItem('localidade')}</li>
          <li>Estado: {localStorage.getItem('uf')}</li>
        </ul>
      </div>
    </div>
  )
}

export default withRouter(Registered)
